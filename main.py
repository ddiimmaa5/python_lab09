from datetime import datetime


class Person:
    def __init__(self, surname, first_name, birth_date, nickname=' '):
        self.surname = surname
        self.first_name = first_name
        self.birth_date = datetime.strptime(birth_date, "%Y-%m-%d")
        self.nickname = nickname

    def get_fullname(self):
        return f'Повне ім\'я: {self.surname} {self.first_name}'

    def get_age(self):
        now = datetime.now()
        res = now.year - self.birth_date.year
        return f'Вік особи в повних роках: {res}'


def modifier(filename):
    filename += '.txt'
    temp = open(filename, 'r').readline()
    res = temp.split()
    new = Person(res[0], res[1], res[2])
    f = open(filename, 'a')
    f.write('\n' + new.get_fullname() + '\n')
    f.write(new.get_age())
    f.close()


new_person = Person("Dominic", "Toretto", "2018-01-31")
fullname = new_person.get_fullname()
age = new_person.get_age()
print(fullname)
print(age)
filename = input('Введіть ім\'я файла: ')
modifier(filename)